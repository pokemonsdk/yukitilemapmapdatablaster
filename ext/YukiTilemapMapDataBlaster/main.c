#include <ruby.h>

#define LiteRGSS2 1

extern void Init_YukiTilemapMapDataBlaster();

VALUE rb_cMapData = Qnil;
VALUE draw_map(int argc, VALUE* argv, VALUE self);

ID rb_idMin = Qundef;
ID rb_idMax = Qundef;
ID rb_idGet = Qundef;
ID rb_idDig = Qundef;
ID rb_idSet = Qundef;

void Init_YukiTilemapMapDataBlaster() {
  VALUE yuki = rb_define_module("Yuki");
  VALUE tilemap = rb_define_class_under(yuki, "Tilemap", rb_cObject);
  rb_cMapData = rb_define_class_under(tilemap, "MapData", rb_cObject);

  rb_define_method(rb_cMapData, "draw_map", draw_map, -1);

  rb_idMin = rb_intern("min");
  rb_idMax = rb_intern("max");
  rb_idGet = rb_intern("[]");
  rb_idDig = rb_intern("dig");
  rb_idSet = rb_intern("set");
}

// LiteRGSS additions
struct rb_Table_Struct_Header {
	unsigned int dim;
	unsigned int xsize;
	unsigned int ysize;
	unsigned int zsize;
	unsigned int data_size;
};

struct rb_Table_Struct {
	struct rb_Table_Struct_Header header;
	short* heap;
};

long tableGetValue3D(VALUE rbtable, long x, long y, long z) {
  struct rb_Table_Struct* table = rb_data_object_get(rbtable);
  if (x < 0 || y < 0 || z < 0 || x >= table->header.xsize || y >= table->header.ysize || z >= table->header.zsize)
    return 0;
  return table->heap[x + (z * table->header.ysize + y) * table->header.xsize];
}

long tableGetValue1D(VALUE rbtable, long x) {
  struct rb_Table_Struct* table = rb_data_object_get(rbtable);
  if (x >= table->header.xsize || x < 0)
    return 0;
  return table->heap[x];
}

void setRect(VALUE rbrect, long x, long y) {
#ifndef LiteRGSS2
  int* rect = rb_data_object_get(rbrect); // Thoerically works according to Cheat Engine research, in case of crash fix this
  rect[0] = x;
  rect[1] = y;
#else
  int* rect = ((int**)rb_data_object_get(rbrect))[1];
  rect[1] = x;
  rect[2] = y;
#endif
}

// Actual Code
VALUE draw_map(int argc, VALUE* argv, VALUE self) {
  VALUE rbx, rby, rbrx, rbry, rblayers;
  rb_scan_args(argc, argv, "50", &rbx, &rby, &rbrx, &rbry, &rblayers);
  long x = NUM2LONG(rbx);
  long y = NUM2LONG(rby);
  long rx = NUM2LONG(rbrx);
  long ry = NUM2LONG(rbry);

  // Start of Ruby Part
  /*
        lx = x_range.min
        mx = x_range.max
        ly = y_range.min
        my = y_range.max
  */
  rbx = rb_iv_get(self, "@x_range");
  long lx = NUM2LONG(rb_funcall(rbx, rb_idMin, 0));
  long mx = NUM2LONG(rb_funcall(rbx, rb_idMax, 0));
  rbx = rb_iv_get(self, "@y_range");
  long ly = NUM2LONG(rb_funcall(rbx, rb_idMin, 0));
  long my = NUM2LONG(rb_funcall(rbx, rb_idMax, 0));

  /*
        bx = lx > x ? lx : x
        ex = mx > rx ? rx : mx
        by = ly > y ? ly : y
        ey = my > ry ? ry : my
        return unless bx <= ex && by <= ey
  */
  long bx = lx > x ? lx : x;
  long ex = mx > rx ? rx : mx;
  long by = ly > y ? ly : y;
  long ey = my > ry ? ry : my;

  if (!(bx <= ex && by <= ey)) {
    return self;
  }

  long ax, ay, tz, tile_id;
  long offset_x = NUM2LONG(rb_iv_get(self, "@offset_x"));
  long offset_y = NUM2LONG(rb_iv_get(self, "@offset_y"));
  VALUE data = rb_iv_get(self, "@data");
  VALUE priorities = rb_iv_get(self, "@priorities");
  VALUE rect = rb_iv_get(self, "@rect");
  VALUE autotiles = rb_iv_get(self, "@autotiles");
  VALUE autotile_c = rb_iv_get(self, "@autotile_counter");
  VALUE tilesets = rb_iv_get(self, "@tilesets");
  VALUE args[3];

  // Loop section
  /*
        bx.upto(ex) do |ax|
          by.upto(ey) do |ay|
            layers.each_with_index do |layer, tz|
              draw(x, y, ax - x, ay - y, tz, layer)
            end
          end
        end
  */
  for(ax = bx; ax <= ex; ax++) {
    for(ay = by; ay <= ey; ay++) {
      for(tz = 0; tz < 3; tz++) {
        /*
          tile_id = self[x + tx, y + ty, tz]
          return unless tile_id && tile_id != 0 # <= It's inside a method called inside this loop

          priority = @priorities[tile_id] || 0
          if tile_id < 384 # Autotile
            layer.dig(priority, ty).set(tx, @autotiles[tile_id / 48 - 1],
                                        @rect.set((tile_id % 48) * 32, @autotile_counter[tile_id / 48] * 32))
          else
            tile_id -= 384
            layer.dig(priority, ty).set(tx, @tilesets[tile_id / 256],
                                        @rect.set(tile_id % 8 * 32, (tile_id % 256) / 8 * 32))
        */

        tile_id = tableGetValue3D(data, ax + offset_x, ay + offset_y, tz);
        if(tile_id == 0) continue;

        rbry = rb_ary_entry(rb_ary_entry(rb_ary_entry(rblayers, tz), tableGetValue1D(priorities, tile_id)), ay - y);
        if (tile_id < 384) { // Autotile
          rbrx = rb_ary_entry(autotile_c, tile_id / 48);
          setRect(rect, (tile_id % 48) * 32, FIX2LONG(rbrx) * 32);
          args[0] = LONG2FIX(ax - x); args[1] = rb_ary_entry(autotiles, tile_id / 48 - 1); args[2] = rect;
        } else {
          tile_id -= 384;
          setRect(rect, (tile_id % 8) * 32, ((tile_id % 256) / 8) * 32);
          args[0] = LONG2FIX(ax - x); args[1] = rb_ary_entry(tilesets, tile_id / 256); args[2] = rect;
        }
        if(args[1] != Qnil) { rb_funcall2(rbry, rb_idSet, 3, args); }
      }
    }
  }
  
  return self;
}

#ifdef NEVER_DEFINED

  // Loop section
  /*
        bx.upto(ex) do |ax|
          by.upto(ey) do |ay|
            layers.each_with_index do |layer, tz|
              draw(x, y, ax - x, ay - y, tz, layer)
            end
          end
        end
  */
  for(ax = bx; ax <= ex; ax++) {
    for(ay = by; ay <= ey; ay++) {
      for(tz = 0; tz < 3; tz++) {
        /*
        tile_id = self[x + tx, y + ty, tz] ; tx = ax -x; ty = ay - y; @data[x + @offset_x, y + @offset_y, z]
        */
        args[0] = LONG2FIX(ax + offset_x); args[1] = LONG2FIX(ay + offset_y); args[2] = LONG2FIX(tz);
        rbry = rb_funcall2(data, rb_idGet, 3, args);
        // rbry = rb_funcall(data, rb_idGet, 3, LONG2FIX(ax + offset_x), LONG2FIX(ay + offset_y), LONG2FIX(tz));
        /*
        return unless tile_id && tile_id != 0
        */
        if(rbry == Qnil) continue;
        tile_id = FIX2LONG(rbry);
        if(tile_id == 0) continue;
        
        /*
        priority = @priorities[tile_id] || 0
        */
        args[0] = LONG2FIX(tile_id);
        rbry = rb_funcall2(priorities, rb_idGet, 1, args);
        // rbry = rb_funcall(priorities, rb_idGet, 1, LONG2FIX(tile_id));
        rbry = rb_ary_entry(rb_ary_entry(rb_ary_entry(rblayers, tz), rbry == Qnil ? 0 : FIX2LONG(rbry)), ay - y);
        if (tile_id < 384) { // Autotile
          rbrx = rb_ary_entry(autotile_c, tile_id / 48);
          args[0] = LONG2FIX((tile_id % 48) * 32); args[1] = LONG2FIX(FIX2LONG(rbrx) * 32);
          rb_funcall2(rect, rb_idSet, 2, args);
          // rb_funcall(rect, rb_idSet, 2, LONG2FIX((tile_id % 48) * 32), LONG2FIX(FIX2LONG(rbrx) * 32));
          args[0] = LONG2FIX(ax - x); args[1] = rb_ary_entry(autotiles, tile_id / 48 - 1); args[2] = rect;
          rb_funcall2(rbry, rb_idSet, 3, args);
          // rb_funcall(rbry, rb_idSet, 3, LONG2FIX(ax - x), rb_ary_entry(autotiles, tile_id / 48 - 1), rect);
        } else {
          tile_id -= 384;
          args[0] = LONG2FIX((tile_id % 8) * 32); args[1] = LONG2FIX(((tile_id % 256) / 8) * 32);
          rb_funcall2(rect, rb_idSet, 2, args);
          // rb_funcall(rect, rb_idSet, 2, LONG2FIX((tile_id % 8) * 32), LONG2FIX(((tile_id % 256) / 8) * 32));
          args[0] = LONG2FIX(ax - x); args[1] = rb_ary_entry(tilesets, tile_id / 256); args[2] = rect;
          rb_funcall2(rbry, rb_idSet, 3, args);
          // rb_funcall(rbry, rb_idSet, 3, LONG2FIX(ax - x), rb_ary_entry(tilesets, tile_id / 256), rect);
        }
      }
    }
  }



  
  // Loop section
  /*
        bx.upto(ex) do |ax|
          by.upto(ey) do |ay|
            layers.each_with_index do |layer, tz|
              draw(x, y, ax - x, ay - y, tz, layer)
            end
          end
        end
  */
  for(ax = bx; ax <= ex; ax++) {
    for(ay = by; ay <= ey; ay++) {
      /*
      tile_id = self[x + tx, y + ty, tz] ; tx = ax -x; ty = ay - y; @data[x + @offset_x, y + @offset_y, z]
      */
      args[0] = LONG2FIX(ax + offset_x); args[1] = LONG2FIX(ay + offset_y); args[2] = 1; // 1 instead of LONG2FIX(0)
      rbry = rb_funcall2(data, rb_idGet, 3, args);
      // rbry = rb_funcall(data, rb_idGet, 3, LONG2FIX(ax + offset_x), LONG2FIX(ay + offset_y), LONG2FIX(tz));
      /*
      return unless tile_id && tile_id != 0
      */
      if(rbry != Qnil && (tile_id = FIX2LONG(rbry)) != 0) {
        /*
        priority = @priorities[tile_id] || 0
        */
        args[0] = LONG2FIX(tile_id);
        rbry = rb_funcall2(priorities, rb_idGet, 1, args);
        // rbry = rb_funcall(priorities, rb_idGet, 1, LONG2FIX(tile_id));
        rbry = rb_ary_entry(rb_ary_entry(rb_ary_entry(rblayers, 0), rbry == Qnil ? 0 : FIX2LONG(rbry)), ay - y);
        if (tile_id < 384) { // Autotile
          rbrx = rb_ary_entry(autotile_c, tile_id / 48);
          args[0] = LONG2FIX((tile_id % 48) * 32); args[1] = LONG2FIX(FIX2LONG(rbrx) * 32);
          rb_funcall2(rect, rb_idSet, 2, args);
          // rb_funcall(rect, rb_idSet, 2, LONG2FIX((tile_id % 48) * 32), LONG2FIX(FIX2LONG(rbrx) * 32));
          args[0] = LONG2FIX(ax - x); args[1] = rb_ary_entry(autotiles, tile_id / 48 - 1); args[2] = rect;
          rb_funcall2(rbry, rb_idSet, 3, args);
          // rb_funcall(rbry, rb_idSet, 3, LONG2FIX(ax - x), rb_ary_entry(autotiles, tile_id / 48 - 1), rect);
        } else {
          tile_id -= 384;
          args[0] = LONG2FIX((tile_id % 8) * 32); args[1] = LONG2FIX(((tile_id % 256) / 8) * 32);
          rb_funcall2(rect, rb_idSet, 2, args);
          // rb_funcall(rect, rb_idSet, 2, LONG2FIX((tile_id % 8) * 32), LONG2FIX(((tile_id % 256) / 8) * 32));
          args[0] = LONG2FIX(ax - x); args[1] = rb_ary_entry(tilesets, tile_id / 256); args[2] = rect;
          rb_funcall2(rbry, rb_idSet, 3, args);
          // rb_funcall(rbry, rb_idSet, 3, LONG2FIX(ax - x), rb_ary_entry(tilesets, tile_id / 256), rect);
        }
      }
      //------
      args[0] = LONG2FIX(ax + offset_x); args[1] = LONG2FIX(ay + offset_y); args[2] = 3; // 3 instead of LONG2FIX(1)
      rbry = rb_funcall2(data, rb_idGet, 3, args);
      // rbry = rb_funcall(data, rb_idGet, 3, LONG2FIX(ax + offset_x), LONG2FIX(ay + offset_y), LONG2FIX(tz));
      if(rbry != Qnil && (tile_id = FIX2LONG(rbry)) != 0) {
        args[0] = LONG2FIX(tile_id);
        rbry = rb_funcall2(priorities, rb_idGet, 1, args);
        // rbry = rb_funcall(priorities, rb_idGet, 1, LONG2FIX(tile_id));
        rbry = rb_ary_entry(rb_ary_entry(rb_ary_entry(rblayers, 1), rbry == Qnil ? 0 : FIX2LONG(rbry)), ay - y);
        if (tile_id < 384) { // Autotile
          rbrx = rb_ary_entry(autotile_c, tile_id / 48);
          args[0] = LONG2FIX((tile_id % 48) * 32); args[1] = LONG2FIX(FIX2LONG(rbrx) * 32);
          rb_funcall2(rect, rb_idSet, 2, args);
          // rb_funcall(rect, rb_idSet, 2, LONG2FIX((tile_id % 48) * 32), LONG2FIX(FIX2LONG(rbrx) * 32));
          args[0] = LONG2FIX(ax - x); args[1] = rb_ary_entry(autotiles, tile_id / 48 - 1); args[2] = rect;
          rb_funcall2(rbry, rb_idSet, 3, args);
          // rb_funcall(rbry, rb_idSet, 3, LONG2FIX(ax - x), rb_ary_entry(autotiles, tile_id / 48 - 1), rect);
        } else {
          tile_id -= 384;
          args[0] = LONG2FIX((tile_id % 8) * 32); args[1] = LONG2FIX(((tile_id % 256) / 8) * 32);
          rb_funcall2(rect, rb_idSet, 2, args);
          // rb_funcall(rect, rb_idSet, 2, LONG2FIX((tile_id % 8) * 32), LONG2FIX(((tile_id % 256) / 8) * 32));
          args[0] = LONG2FIX(ax - x); args[1] = rb_ary_entry(tilesets, tile_id / 256); args[2] = rect;
          rb_funcall2(rbry, rb_idSet, 3, args);
          // rb_funcall(rbry, rb_idSet, 3, LONG2FIX(ax - x), rb_ary_entry(tilesets, tile_id / 256), rect);
        }
      }
      //------
      args[0] = LONG2FIX(ax + offset_x); args[1] = LONG2FIX(ay + offset_y); args[2] = 5; // 5 instead of LONG2FIX(2)
      rbry = rb_funcall2(data, rb_idGet, 3, args);
      // rbry = rb_funcall(data, rb_idGet, 3, LONG2FIX(ax + offset_x), LONG2FIX(ay + offset_y), LONG2FIX(tz));
      if(rbry != Qnil && (tile_id = FIX2LONG(rbry)) != 0) {
        args[0] = LONG2FIX(tile_id);
        rbry = rb_funcall2(priorities, rb_idGet, 1, args);
        // rbry = rb_funcall(priorities, rb_idGet, 1, LONG2FIX(tile_id));
        rbry = rb_ary_entry(rb_ary_entry(rb_ary_entry(rblayers, 2), rbry == Qnil ? 0 : FIX2LONG(rbry)), ay - y);
        if (tile_id < 384) { // Autotile
          rbrx = rb_ary_entry(autotile_c, tile_id / 48);
          args[0] = LONG2FIX((tile_id % 48) * 32); args[1] = LONG2FIX(FIX2LONG(rbrx) * 32);
          rb_funcall2(rect, rb_idSet, 2, args);
          // rb_funcall(rect, rb_idSet, 2, LONG2FIX((tile_id % 48) * 32), LONG2FIX(FIX2LONG(rbrx) * 32));
          args[0] = LONG2FIX(ax - x); args[1] = rb_ary_entry(autotiles, tile_id / 48 - 1); args[2] = rect;
          rb_funcall2(rbry, rb_idSet, 3, args);
          // rb_funcall(rbry, rb_idSet, 3, LONG2FIX(ax - x), rb_ary_entry(autotiles, tile_id / 48 - 1), rect);
        } else {
          tile_id -= 384;
          args[0] = LONG2FIX((tile_id % 8) * 32); args[1] = LONG2FIX(((tile_id % 256) / 8) * 32);
          rb_funcall2(rect, rb_idSet, 2, args);
          // rb_funcall(rect, rb_idSet, 2, LONG2FIX((tile_id % 8) * 32), LONG2FIX(((tile_id % 256) / 8) * 32));
          args[0] = LONG2FIX(ax - x); args[1] = rb_ary_entry(tilesets, tile_id / 256); args[2] = rect;
          rb_funcall2(rbry, rb_idSet, 3, args);
          // rb_funcall(rbry, rb_idSet, 3, LONG2FIX(ax - x), rb_ary_entry(tilesets, tile_id / 256), rect);
        }
      }
    }
#endif
